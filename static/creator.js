/*
Mellis
Copyright (C) 2021 Felix v. Oertzen
mellis@von-oertzen-berlin.de

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
function never(){
  exp = document.getElementById("expiration");
  exp.value = getMeta("NEVER");
}

function checkLogin() {
  if ((getMeta("createhoneypot") == "true") && (getMeta("userauth") == "false"))
    snackbar("auth_failed");
  if (getMeta("honeypotexistsalready") == "true")
    snackbar("link_already_there");
}

document.addEventListener('DOMContentLoaded', function () {
  document.getElementById('copytoclipboard')
    .addEventListener('click', function() {
       copyTextToClipboard(getData("#copytoclipboard", "url"));
    });
    checkLogin();
});
