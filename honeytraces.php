<?php /*
Mellis
Copyright (C) 2021 Felix v. Oertzen
mellis@von-oertzen-berlin.de

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
require_once("config.php");

function footer() {
  ?>
  <footer><a href="http<?php echo $_SERVER['HTTPS'] ? "s":""; ?>://<?php echo $_SERVER['HTTP_HOST'];?>/">Mellis - honeypot generator</a></footer>
  <?php
}

function getSubString($str, $substr, $length) {
  return substr($str, strpos($str, $substr) + strlen($substr), $length);
}
function getSubStringUntil($str, $substr, $until) {
  $beginning = strpos($str, $substr) + strlen($substr);
  return substr($str, $beginning, strpos($str, $until, $beginning) - $beginning);
}


function getBrowser($useragent) {
  $os = "";
  // get os/plattform
  if (strpos($useragent, "Android")) {
    $os = getSubString($useragent, "Android", 100);
    $os = substr($os, 0, strpos($os, ";") < -strpos($os, ")") ? strpos($os,")") : strpos($os,";"));
    $os = str_replace("_", ".", $os);
    $os = "Android " . $os;
  } else if (strpos($useragent, "Linux")) {
    $os = "Linux";
  } else if (strpos($useragent, "Windows NT 10")) {
    $os = "Windows 10";
  } else if (strpos($useragent, "Windows NT 6.3")) {
    $os = "Windows 8.1";
  } else if (strpos($useragent, "Windows NT 6.1")) {
    $os = "Windows 7";
  } else if (strpos($useragent, "iPhone")) {
    $os = getSubString($useragent, "iPhone OS ", 100);
    $os = substr($os, 0, strpos($os," "));
    $os = str_replace("_", ".", $os);
    $os = "iOS " . $os . " (iPhone)";
  } else if (strpos($useragent, "iPad")) {
      $os = getSubString($useragent, "iPhone OS ", 100);
      $os = substr($os, 0, strpos($os," "));
      $os = str_replace("_", ".", $os);
      $os = "iPadOS " . $os;
  } else if (strpos($useragent, "Mac OS X")) {
    $os = getSubString($useragent, "Mac OS X ", 100);
    $os = substr($os, 0, strpos($os, ";") < -strpos($os, ")") ? strpos($os,")") : strpos($os,";"));
    $os = str_replace("_", ".", $os);
    $os = "macOS " . $os;
  }

  $summary = $os;
  if (!$os) {
    return $useragent;
  }
  // get browser
  if (strpos($useragent, "Firefox")) {
    $version = getSubString($useragent, "Firefox/", 4);
    return "Firefox " . $version . " on " . $os;
  } else if (strpos($useragent, "Chrome")) {
    $version = getSubString($useragent, "Chrome/", 4);
    return "Chrome " . $version . " on " . $os;
  } else if (strpos($useragent, "Safari")) {
    $version = getSubStringUntil($useragent, "Version/", " ");
    return "Safari " . $version . " on " . $os;
  }
  return $summary ? $summary : $useragent;
}

$db = new PDO("sqlite:" . $CONFIG['db']);
if (password_verify($_POST['loginsecret'], password_hash($CONFIG['password'], PASSWORD_DEFAULT))) {
  if (isset($_POST['delete'])) {
    $s1 = $db->prepare("DELETE FROM honeypot WHERE id=?");
    $s2 = $db->prepare("DELETE FROM honeytrace WHERE tracer=?");
    $s1->bindParam(1, htmlentities($_POST['delete']));
    $s2->bindParam(1, htmlentities($_POST['delete']));
    $s1->execute();
    $s2->execute();
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
<?php echo "<meta name=\"honeypotdeleted\" content=\"" . (isset($_POST['delete']) ? "true" : "false") . "\""; ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mellis</title>
    <link href="static/stylesheets/general.css" rel="stylesheet" type="text/css" />
    <link href="static/stylesheets/honeytraces.css" rel="stylesheet" type="text/css" />
    <link href="static/stylesheets/snackbar.css" rel="stylesheet" type="text/css" />
    <link href="static/stylesheets/tooltip.css" rel="stylesheet" type="text/css" />
    <link href="static/stylesheets/styles.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="static/utils.js"></script>
    <script type="text/javascript" src="static/moment.min.js"></script>
  </head>
  <body>
    <h2>Mellis - see "your" data</h2>
    <?php   foreach ($db->query("SELECT * FROM honeypot;") as $results) { ?>
      <div class="container">
        <h3><?php echo $results['friendlyname']; ?> - <code><?php echo  $CONFIG['installationroot'] . $results['url']; ?> &rarr; <?php echo $results['destination']; ?></code></h3>
        <?php echo $results['description'];?>
        <form method="post" action="honeytraces.php">
          <input type="hidden" value="<?php echo $_POST['loginsecret'];?>" name="loginsecret">
          <input type="hidden" value="<?php echo $results['id'];?>" name="delete">
          <input type="submit" value="DELETE">
        </form>
        <table>
          <thead>
            <tr>
              <th class="th_time_date"></th>
              <th class="th_internet_protocoll"></th>
              <th class="th_host_name"></th>
              <th class="th_user_agent"></th>
            </tr>
          </thead>
          <tbody>
            <?php $datastmt = $db->prepare("SELECT * FROM honeytrace WHERE tracer = ?");
            $datastmt->bindParam(1, $results['id']);
            if ($datastmt->execute()) {
              foreach ($datastmt->fetchAll() as $traces) {
                $dateObj = DateTime::CreateFromFormat('Y-m-d H:i:s',$traces['effectivetime']);
                if ($traces['host'] == "no hostname found") {$traces['host'] = "&mdash;";}
                echo "<tr> <td>"
                . $dateObj->Format('D\, j. M y, H:i')."</td><td>"
                . $traces['ip'] . "</td><td>"
                . $traces['host'] . "</td><td class=\"tooltip\">"
                . getBrowser($traces['useragent']) .
                " <span class=\"tooltiptext\">" . $traces['useragent'] . "</span></td> </tr>";
              }
            }
            ?>
        </tbody>
      </table>
      </div>
    <?php }?>
    <div id="snackbar_deleted" class="snackbar">honeypot deleted</div>
  </body>
  <script type="text/javascript" src="static/honeytraces.js"></script>
</html>

<?php
//TODO
//} else {
//  header("location: creator.php?passwordchallange=failed");
//  echo "password challenge failed.";
}
?>
